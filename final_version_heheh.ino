// #include <SoftwareSerial.h>

//define pin numbers:
const int trigLeft = 10;
const int echoLeft = 9;
const int trigFront = 8;
const int echoFront = 7;
const int S0 = 5;
const int S1 = 6;
const int S2 = 3;
const int S3 = 4;
const int sensorOut = 2;
const int LED = 13;
// SoftwareSerial hm10Serial(2, 3);

// defines variables
int MotorLSpeed = 150;//laptop: 150;
int MotorRSpeed = 130;//laptop: 130;
int enough = 20; //distance deemed great enough to be worthy of a turn
int notEnough = 5;
int reverseDelay = 200;
int MotorDelay = 50; //laptop: 100;
int stopDelay = 500;
int spinDelay = 500; //laptop: 1000; //how long it takes to spin
int turnDelay = 250 ;//laptop: 500; //how long it takes to turn
int numBlinks = 5;
int ledDelay = 250;
long duration;
int distance;
unsigned long startTime;
const unsigned long timeoutDuration = 2000;

typedef struct {
    int In1;
    int In2;
    int Enable;
} motor;

// ultrasonic_sensor UltrasonicSensor = {};
motor MotorL = {};
motor MotorR = {};

void distances(int* distanceL, int* distanceF) { //set longer pulse times
  // Clears the trigPin
  digitalWrite(trigLeft, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigLeft, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigLeft, LOW);

  // Reads the echoPin, returns the sound wave travel time in microseconds
  int durationL = pulseIn(echoLeft, HIGH);
  // Calculating the distance
  *distanceL = durationL*0.034/2;

  digitalWrite(trigFront, LOW);
  delayMicroseconds(2);
  digitalWrite(trigFront, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigFront, LOW);
  int durationF = pulseIn(echoFront, HIGH);
  *distanceF = durationF*0.034/2;

  Serial.println("Distance:");
  Serial.println(*distanceF); Serial.println(*distanceL);

  // return distances
  // return distanceL, distanceF;
}


char findColour() { //uses colour sensor, returns colour
  
  // initialise the frequency variable and the RGB variables
  int R = 0; int G = 0; int B = 0;
  int R1 = 0; int R2 = 0; int R3 = 0; int G1 = 0; int G2 = 0; int G3 = 0; int B2 = 0; int B3 = 0; int b1 = 0;
  // intialise the final value
  char color;
  // Setting red filtered photodiodes to be read
  digitalWrite(S2,LOW); digitalWrite(S3,LOW);
  R = pulseIn(sensorOut, LOW); 
  // R1 = pulseIn(sensorOut, LOW); 
  // R2 = pulseIn(sensorOut, LOW);
  // R3 = pulseIn(sensorOut, LOW);
  // R = (R1 + R2 + R3)/3;
  Serial.print("R="); Serial.print(R); Serial.print(" ");
  delay(100);
  // Setting Green filtered photodiodes to be read
  digitalWrite(S2,HIGH); digitalWrite(S3,HIGH);
  G = pulseIn(sensorOut, LOW); 
  // G1 = pulseIn(sensorOut, LOW); 
  // G2 = pulseIn(sensorOut, LOW);
  // G3 = pulseIn(sensorOut, LOW);
  // G = (G1 + G2 + G3)/3;
  Serial.print("G="); Serial.print(G); Serial.print(" ");
  delay(100);
  // Setting Blue filtered photodiodes to be read
  digitalWrite(S2,LOW); digitalWrite(S3,HIGH);
  B = pulseIn(sensorOut, LOW);
  // b1 = pulseIn(sensorOut, LOW);
  // B2 = pulseIn(sensorOut, LOW);
  // B3 = pulseIn(sensorOut, LOW);
  // B = (b1 + B2 + B3)/3;
  Serial.print("B="); Serial.print(B); Serial.print(" ");
  delay(100);
  // choose one of the seven colors between a certain range of numbers (0 to 255)
  if (R>=130 && R<=160 && G>=175 && G<=210 && B>=70 && B<=185) {
    Serial.println("white");
   // white
    return '0';
  } else if (R >= 40 && R <= 85 && G >= 150 && G <=210 && B >= 105 && B <= 150) {
    Serial.println("red"); // R>=25 && R<=50 && G>=110 && G<=140 && B>=75 && B<=110
    // red
    return '6';
  } else if (R>=60 && R<=110 && G>=55 && G<=115 && B>=85 && B<=145) {
    Serial.println("green");
    // green
    return '3';
  } else if (R>=60 && R<=70 && G>=95 && G<=110 && B>=40 && B<=60) {
    Serial.println("purple");
    // purple
    return '4';
  } else if (R >= 130 && R <= 160 && G >= 80 && G <=105 && B>=30 && B<=50) {
    Serial.println("blue");
    // blue
    return '2';
  } else if (R>=20 && R<=50 && G>=30 && G<=70 && B>=60 && B<=120) {
    Serial.println("yellow");
    // yellow
    return '5';
  } else if (R>=180 && R<=205 && G>=250 && G<=270 && B>=190 && B<=220) {
    Serial.println("black");
    // black
    return '1';
  } else {
    Serial.println("N/A");
    // N/A
    return '7';
  }
  Serial.println("");
}


char concurrentColour(char currentColour) { //loops until a new colour is reached
  bool loopFinished = false; //goes until new colour is met
  char colour;
  while (!loopFinished) {
    colour = findColour();
    if (colour != currentColour) {
      Serial.println('new colour spotted!');
      loopFinished = true;
    }
    else if (millis() - startTime >= timeoutDuration) { //in case it doesn't detect moving to next cell
      Serial.println('timed out - moving on');
      loopFinished = true;
    } 
    else if (colour == '7') {
       //
    }
    else {
      Serial.println('still checking');
    }
  }
  char newColour = colour;
  return newColour;
}

void lightUp(int delayTime, int blinkCount) {
  for (int i = 0; i < blinkCount; i++) {
    digitalWrite(LED, HIGH);  // Turn the LED on
    delay(delayTime);            // Wait for the specified delay time
    digitalWrite(LED, LOW);   // Turn the LED off
    delay(delayTime);            // Wait again
  }
}

// void openGates() {

// }

void goBackward(int motorLSpeed, int motorRSpeed) {
  digitalWrite(MotorL.In1, HIGH);
  digitalWrite(MotorL.In2, LOW);
  analogWrite(MotorL.Enable, motorLSpeed);
  digitalWrite(MotorR.In1, HIGH);
  digitalWrite(MotorR.In2, LOW);
  analogWrite(MotorR.Enable, motorRSpeed);
}

void goForward(int motorLSpeed, int motorRSpeed) {
  digitalWrite(MotorL.In1, LOW);
  digitalWrite(MotorL.In2, HIGH);
  analogWrite(MotorL.Enable, motorLSpeed);
  digitalWrite(MotorR.In1, LOW);
  digitalWrite(MotorR.In2, HIGH);
  analogWrite(MotorR.Enable, motorRSpeed);
}

void turnLeft(int motorLSpeed, int motorRSpeed) {
  digitalWrite(MotorL.In1, LOW);
  digitalWrite(MotorL.In2, HIGH);
  analogWrite(MotorL.Enable, motorLSpeed);
  digitalWrite(MotorR.In1, HIGH);
  digitalWrite(MotorR.In2, LOW);
  analogWrite(MotorR.Enable, motorRSpeed);
}

void turnRight(int motorLSpeed, int motorRSpeed) {
  digitalWrite(MotorL.In1, HIGH);
  digitalWrite(MotorL.In2, LOW);
  analogWrite(MotorL.Enable, motorLSpeed);
  digitalWrite(MotorR.In1, LOW);
  digitalWrite(MotorR.In2, HIGH);
  analogWrite(MotorR.Enable, motorRSpeed);

}

void stop() {
  digitalWrite(MotorL.In1, HIGH);
  digitalWrite(MotorL.In2, HIGH);
  // analogWrite(MotorL.Enable, 0);
  digitalWrite(MotorR.In1, HIGH);
  digitalWrite(MotorR.In2, HIGH);
  // analogWrite(MotorR.Enable, 0);
}


void setup() {
  Serial.begin(9600);
  int frequency = 0;

  //COLOUR SENSOR
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);
  digitalWrite(S0,HIGH);
  digitalWrite(S1,LOW);

  //LED 
  pinMode(LED, OUTPUT);

  //BLUETOOTH


  // ULTRASONIC SENSOR
  pinMode(trigLeft, OUTPUT);
  pinMode(echoLeft, INPUT);
  pinMode(trigFront, OUTPUT);
  pinMode(echoFront, INPUT);
  
  // MOTORS
  MotorL.In1 = A4; // 2
  MotorL.In2 = A2; // 4
  MotorL.Enable = A5; // 5
  pinMode(MotorL.In1, INPUT);
  pinMode(MotorL.In2, INPUT);
  pinMode(MotorL.Enable, INPUT);
  
  MotorR.In1 = A1;
  MotorR.In2 = A3;
  MotorR.Enable = A0; // 3
  pinMode(MotorR.In1, INPUT);
  pinMode(MotorR.In2,  INPUT);
  pinMode(MotorR.Enable, INPUT);


}

void loop() {
  // check current cell colour
  char currentCol = findColour();
  delay(200);

  //set motors to move forward
  goForward(MotorLSpeed, MotorRSpeed);
  delay(MotorDelay);

  //continuously check colour against colour of initial cell
  char oldColour = currentCol;
  startTime = millis();
  char newColour = concurrentColour(currentCol);

  //stop once a new colour is reached
  stop();
  if (newColour == '3') { //red - need to perform a 180 & light up LED
    lightUp(ledDelay, numBlinks);
    goBackward(MotorLSpeed, MotorRSpeed);
    delay(reverseDelay);
    turnRight(MotorLSpeed, MotorRSpeed);
    delay(spinDelay);
    stop();
    delay(stopDelay);
  }
  else {
    //using ultrasonic sensors to find best path
    int* left; int* forward;
    distances(left, forward);
    if (*left > enough) {
      turnLeft(MotorLSpeed, MotorRSpeed); //make left turn
      delay(turnDelay);
      stop();
      delay(stopDelay);
    }
    else if (*forward > enough) {
    //leave empty, nothing to execute until the next loop
      stop(); //be careful of this, it has already stopped
      delay(stopDelay);
    }
    else if (*forward < notEnough && *left > notEnough) { //if not super close to front, but very close to left, turn right
      turnRight(MotorLSpeed, MotorRSpeed); 
      delay(turnDelay);
      stop();
      delay(stopDelay);
    }
    else if (*forward < notEnough && *left < notEnough) { //if too close, reverse then do a 180
      goBackward(MotorLSpeed, MotorRSpeed);
      delay(reverseDelay);
      turnRight(MotorLSpeed, MotorRSpeed);
      delay(spinDelay);
      stop();
      delay(stopDelay);
    }
    else {
      turnRight(MotorLSpeed, MotorRSpeed); //do a 180 if not super close
      delay(spinDelay);
      stop();
      delay(stopDelay);
    }
  }
  delay(100);
};
